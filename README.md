# Teacher Information Management Application



## Description

This console application is designed to manage information about teachers. It allows you to create and manage records for teachers, including their names, birthdates, and basic salaries. The application also calculates the total salary for each teacher, taking into account a salary coefficient specific to teachers.

## Features

- Create, edit, and delete teacher records.
- Calculate and display the total salary for each teacher based on a salary coefficient.
- Find and display information about the teacher with the lowest salary.

## Installation

- Clone this repository to your local machine using git clone.
- Navigate to the project directory.
- Compile the C# code using a C# compiler (e.g., Visual Studio or the .NET CLI).
- Run the compiled application on your console.

## Usage

    To use the Teacher Information Management Console Application, follow these steps:
    - Launch the application.
    - Choose from the available options to create, edit, delete, or display teacher records.
    - Follow the prompts to input teacher information.
    - The application will calculate and display salary information as required.
    - Support
    - If you encounter any issues or have questions about this application, please feel free to open an issue on the GitHub repository.

## Support

If you encounter any issues or have questions about this application, please feel free to open an issue on the GitLab repository.

## Roadmap

- Add the ability to save and load teacher records from a file.
- Implement additional salary calculation methods.
- Improve the user interface for a better user experience.

## Contributing
    We welcome contributions from the community! If you'd like to contribute to this project, please follow these steps:
    -     Fork the repository.
    -     Create a new branch for your feature or bug fix.
    -     Make your changes and commit them with descriptive commit messages.
    -     Push your changes to your forked repository.
    -     Create a pull request to the main repository.

## Authors and Acknowledgment 
This project was created by _@thangnguyen7410_.

## Project Status
Development of this project is ongoing. We are open to contributions and would appreciate your help in making it even better. If you are interested in becoming a maintainer, please reach out to us.
