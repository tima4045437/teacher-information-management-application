﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIMA;

namespace TIMA
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Nhap so luong giao vien: ");
            int n = int.Parse(Console.ReadLine());

            List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

            for (int i = 0; i < n; i++)
            {
                GiaoVien giaoVien = new GiaoVien();
                Console.WriteLine($"Nhap thong tin giao vien {i + 1}:");
                giaoVien.NhapThongTin();
                danhSachGiaoVien.Add(giaoVien);
            }

            GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];

            foreach (var gv in danhSachGiaoVien)
            {
                if (gv.TinhLuong() < giaoVienLuongThapNhat.TinhLuong())
                {
                    giaoVienLuongThapNhat = gv;
                }
            }

            Console.WriteLine("Thong tin giao vien co luong thap nhat:");
            giaoVienLuongThapNhat.XuatThongTin();

            Console.WriteLine("Nhan mot phim bat ky de thoat...");
            Console.ReadKey();
        }
    }
}
