﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIMA
{
    class NguoiLaoDong
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public double LuongCoBan { get; set; }

        public NguoiLaoDong()
        {
        }

        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public void NhapThongTin()
        {
            Console.Write("Nhap Ho Ten: ");
            HoTen = Console.ReadLine();
            Console.Write("Nhap Nam Sinh: ");
            NamSinh = int.Parse(Console.ReadLine());
            Console.Write("Nhap Luong Co Ban: ");
            LuongCoBan = double.Parse(Console.ReadLine());
        }

        public double TinhLuong()
        {
            return LuongCoBan;
        }

        public void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
        }
    }

    class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien()
        {
        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public new void NhapThongTin()
        {
            base.NhapThongTin();
            Console.Write("Nhap He So Luong: ");
            HeSoLuong = double.Parse(Console.ReadLine());
        }

        public new double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine($"He so luong: {HeSoLuong}, Luong: {TinhLuong()}");
        }

        public void XuLy()
        {
            HeSoLuong += 0.6;
        }
    }
}

